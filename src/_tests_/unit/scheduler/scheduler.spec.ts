import { schedule } from '../../../../schedule';

describe('Schedule', function() {
    it('Call Per Minute', function() {
      let result = schedule.call_minute();
      expect(result).toBe('resp minute called');   
    });
});