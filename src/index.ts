var express = require('express')
var app = express()
var scheduler = require('./schedule.ts');

app.get('/pp_day', function (req,res) {
   scheduler.call_day();
  })

app.get('/pp_week', function (req,res) {
	scheduler.call_week();
})

app.get('/pp_month', function (req,res) {

  scheduler.call_month();
})

app.get('/pp_minute', function (req,res) {

	scheduler.call_minute();
})
app.listen(8990, function () {
  console.log("server started");
});