const {defaults} = require('jest-config');

module.exports = {
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testRegex: '(/__tests__/unit/.*)spec.ts$',
};
